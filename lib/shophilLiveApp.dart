import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

import './data/ApiClient.dart';
import './bloc/authenticate/Authenticate.dart';
import './repositories/UserRepository.dart';
import './repositories/AuthenticationRepository.dart';
import './repositories/ShopRepository.dart';
import './repositories/FacebookLiveRepository.dart';
import './repositories/FacebookLiveFeedRepository.dart';
import './repositories/ProductRepository.dart';
import './repositories/FacebookLiveVideoProductRepository.dart';
import './managers/AuthenticationManager.dart';

import './widgets/screens/HomeScreen.dart';

class ShophilLiveApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<FlutterSecureStorage>(
          create: (BuildContext context) {
            return FlutterSecureStorage();
          },
        ),
        RepositoryProvider<FacebookAuth>(
          create: (BuildContext context) {
            return FacebookAuth.instance;
          },
        ),
        RepositoryProvider<ApiClient>(
          create: (BuildContext context) {
            return ApiClient(
              storage: context.read<FlutterSecureStorage>(),
            );
          },
        ),
        RepositoryProvider<UserRepository>(
          create: (BuildContext context) {
            return UserRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<AuthenticationRepository>(
          create: (BuildContext context) {
            return AuthenticationRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<ShopRepository>(
          create: (BuildContext context) {
            return ShopRepository(
              apiClient: context.read<ApiClient>(),
              storage: context.read<FlutterSecureStorage>(),
            );
          }
        ),
        RepositoryProvider<FacebookLiveRepository>(
          create: (BuildContext context) {
            return FacebookLiveRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<FacebookLiveVideoProductRepository>(
          create: (BuildContext context) {
            return FacebookLiveVideoProductRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<FacebookLiveFeedRepository>(
          create: (BuildContext context) {
            return FacebookLiveFeedRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<ProductRepository>(
          create: (BuildContext context) {
            return ProductRepository(
              apiClient: context.read<ApiClient>(),
            );
          }
        ),
        RepositoryProvider<AuthenticationManager>(
          create: (BuildContext context) {
            return AuthenticationManager(
              authenticationRepository: context.read<AuthenticationRepository>(),
              userRepository:context.read<UserRepository>(),
              storage: context.read<FlutterSecureStorage>(),
              facebookAuth: context.read<FacebookAuth>()
            );
          }
        )
      ],
      child: BlocProvider<AuthenticationBloc>(
        create: (context) {
          return AuthenticationBloc(
            userRepository: context.read<UserRepository>(),
            authenticationManager: context.read<AuthenticationManager>(),
          )..add(AppStarted());
        },
        child: MaterialApp(
          title: 'Shophil Live',
          initialRoute: '/',
          routes: {
            '/': (context) => HomeScreen(),
          },
        ),
      )
    );
  }
}