import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';

import '../repositories/UserRepository.dart';
import '../repositories/AuthenticationRepository.dart';

class AuthenticationManager{
  final FlutterSecureStorage storage;
  final AuthenticationRepository authenticationRepository;
  final UserRepository userRepository;
  final FacebookAuth facebookAuth;

  AuthenticationManager({
    required this.storage,
    required this.authenticationRepository,
    required this.userRepository,
    required this.facebookAuth
  });

  Future<String> authenticate({
    required String username,
    required String password
  }) async {
    Map<String, dynamic> json = await authenticationRepository.loginCheck(username: username, password: password);
    String token = json['access_token'];
    await persistToken(token);
    return token;
  }

  Future<String> facebookAuthenticate() async {
    try {
      await facebookAuth.logOut();
    } catch (e) {
    }

    final LoginResult result  = await facebookAuth.login();

    if (result.status == LoginStatus.success) {
      final AccessToken accessToken =  result.accessToken!;
      String facebookToken = accessToken.token;
      Map<String, dynamic> json = await authenticationRepository.facebookAuthenticate(accessToken: facebookToken);
      String token = json['access_token'];
      await persistToken(token);
      return token;
    } else {
      print(result.message);
      throw('เกิดข้อผิดพลาด');
    }
  }

  Future<String> getToken() async {
    String? token = await storage.read(key: 'token');
    if (token == null) {
      throw Exception();
    }
    return token;
  }

  Future<void> persistToken(String token) async {
    await storage.write(key: 'token', value: token);
  }

  Future<void> deleteToken() async {
    await storage.delete(key: 'token');
  }

}