import 'package:flutter/material.dart';

class SecondaryActionButton extends StatelessWidget {

  SecondaryActionButton({required this.child, required this.onPressed, required this.outlineColor, this.buttonWidth});

  // Text or widget to show inside the button
  final Widget child;

  // Callback that fires when the user taps on this button
  final VoidCallback onPressed;

  final Color outlineColor;
  final double? buttonWidth;

  @override
  Widget build(BuildContext context) {

    return Container(
      width: (this.buttonWidth == null) ? double.infinity : this.buttonWidth,
        child: ElevatedButton(
          style: ElevatedButton.styleFrom(
            textStyle: TextStyle(color: Color(0xFF484E57)),
            primary: Colors.white,
            padding: const EdgeInsets.symmetric(vertical: 15),
            shape: RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(28.0),
              side: BorderSide(color: outlineColor)
            ),
          ),
          onPressed: onPressed,
          child: child,
        ),
    );
  }
}