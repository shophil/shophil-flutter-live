import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:loading_animations/loading_animations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:intl/intl.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:quiver/strings.dart';
import 'dart:async';

import '../../bloc/facebookLive/FacebookLive.dart';
import '../../bloc/authLayout/AuthLayoutBloc.dart';
import '../../repositories/FacebookLiveRepository.dart';
import '../../repositories/FacebookLiveFeedRepository.dart';
import '../../repositories/ProductRepository.dart';
import '../../repositories/FacebookLiveVideoProductRepository.dart';
import '../../models/FacebookLiveVideo.dart';
import '../../models/FacebookLiveFeed.dart';
import '../../models/Product.dart';
import '../../models/Item.dart';

import '../common/BottomNavigation.dart';
import '../common/LoadingWidget.dart';
import '../common/DefaulTextInput.dart';
import '../product/RefNoBox.dart';
import '../product/ProductDefaultImageWithShowLiveOverlay.dart';

class Debouncer {
  final int milliseconds;
  Timer? _timer;

  Debouncer({ required this.milliseconds });

  run(VoidCallback action) {
    if (_timer != null) {
      _timer!.cancel();
    }
    _timer = Timer(Duration(milliseconds: milliseconds), action);
  }
}

class LiveScreen extends StatefulWidget {
  LiveScreen({Key? key}) : super(key: key);

  @override
  State<LiveScreen> createState() => _LiveScreenState();
}

class _LiveScreenState extends State<LiveScreen> with SingleTickerProviderStateMixin {
  FacebookLiveVideo? facebookLiveVideo;
  List<FacebookLiveFeed>? facebookLiveFeeds;
  Completer? _completer;

  late ScrollController _scrollController;
  late bool _hasReachMaxRecord;
  late bool _scrollLock;
  final _scrollThreshold = 33.0;
  late TabController _tabController;

  Product? product;
  Product? sellProduct;
  bool hideOverlay = true;

  final _debouncer = Debouncer(milliseconds: 800);

  @override
  void initState() {
    _scrollController = ScrollController();
    _hasReachMaxRecord = false;
    _scrollLock = false;
    _tabController = TabController(initialIndex: 0, length: 2, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    _scrollController.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<FacebookLiveBloc>(
      create: (BuildContext context) {

        final bloc = FacebookLiveBloc(
          authLayoutBloc: context.read<AuthLayoutBloc>(),
          facebookLiveRepository: context.read<FacebookLiveRepository>(),
          facebookLiveFeedRepository: context.read<FacebookLiveFeedRepository>(),
          productRepository: context.read<ProductRepository>(),
          facebookLiveVideoProductRepository: context.read<FacebookLiveVideoProductRepository>()
        )..add(SocketInit())..add(LiveInit());

        _scrollController.addListener(() {
          if (_tabController.index != 0 && MediaQuery.of(context).orientation == Orientation.portrait) {
            return;
          }
          final maxScroll = _scrollController.position.maxScrollExtent;
          final currentScroll = _scrollController.position.pixels;
          if ((_scrollLock == false) && (_hasReachMaxRecord == false) && (maxScroll - currentScroll <= _scrollThreshold)) {
            _scrollLock = true;
            bloc.add(FetchFeedsMore(facebookLiveVideo: facebookLiveVideo!, facebookLiveFeeds: facebookLiveFeeds!));
          }
        });

        return bloc;
      },
      child: BlocConsumer<FacebookLiveBloc, FacebookLiveState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder,
      ),
    );

  }

  void _mainContentListenter(BuildContext context, FacebookLiveState state) async {
    if (state is FacebookLiveLoaded) {
      facebookLiveVideo = state.facebookLiveVideo;
      facebookLiveFeeds = state.facebookLiveFeeds;
      _hasReachMaxRecord = state.hasReachMax;
      _scrollLock = false;
      if (_completer != null && !_completer!.isCompleted) {
        _completer!.complete();
      }
    } else if (state is FacebookLiveFeedSync) {
      FacebookLiveFeed feed = state.facebookLiveFeed;
      bool found = false;
      for (int i = 0; i < facebookLiveFeeds!.length; i++) {
        if (facebookLiveFeeds![i] == feed) {
          facebookLiveFeeds![i] = feed;
          found = true;
        }
      }
      if (found == false) {
        facebookLiveFeeds!.insert(0, feed);
      }
    } else if (state is ProductLoaded) {
      product = state.product;
      if (_completer != null && !_completer!.isCompleted) {
        _completer!.complete();
      }
    } else if (state is ItemSync) {
      if (product != null) {
        for (int i = 0; i < product!.items!.length; i++) {
          if (product!.items![i] == state.item) {
            product!.items![i] = state.item;
          }
        }
      }
    } else if (state is SellProductLoaded) {
      sellProduct = state.product;
    } else if (state is HideShowUpdated) {
      hideOverlay = state.hideOverlay;
    }
  }

  Widget _mainContentBuilder(BuildContext context, FacebookLiveState state) {
    return Scaffold(
      appBar: AppBar(
        title: Text(facebookLiveVideo == null ? '' : '(${facebookLiveVideo!.status}) ${facebookLiveVideo!.title}'),
      ),
      body: Builder(
        builder: (BuildContext context) {
          if (facebookLiveVideo == null && state is FacebookLiveProcessing) {
            return Center(
              child: LoadingBouncingGrid.square(
                size: 100,
                backgroundColor: Color.fromRGBO(34,118,173,1.0)
              )
            );
          }

          if (MediaQuery.of(context).orientation == Orientation.portrait) {
            return GestureDetector(
              onTap: () {
                FocusScope.of(context).unfocus();
              },
              child: TabBarView(
                physics: ClampingScrollPhysics(),
                controller: _tabController,
                children: [
                  liveTap(context, state),
                  productView(context, state)
                ],
              )
            );
          }

          return GestureDetector(
            onTap: () {
              FocusScope.of(context).unfocus();
            },
            child: Row(
              children: [
                Container(
                  width: 0.5.sw,
                  child: liveTap(context, state),
                ),
                Container(
                  width: 0.5.sw,
                  child: productView(context, state)
                ),
              ],
            )
          );
        }
      ),
      bottomNavigationBar: BottomNavigation(currentIndex: 0,),
    );
  }

  Widget productView(BuildContext context, FacebookLiveState state) {
    List<Widget> items = [];

    items.add(
      DefaultTextInput(
        key: Key('Product'),
        label:  'Ref No',
        validator: (value) {
          if (value == null) {
            return null;
          }

          if (value.isEmpty) {
            return 'เกิดข้อผิดพลาด';
          }
          return null;
        },
        onChanged: (value) {
          if (isEmpty(value)) {
            return;
          }
          _debouncer.run(() => {
            print('Process ::: $value'),
            context.read<FacebookLiveBloc>().add(SearchProduct(refno: value, facebookVideoId: facebookLiveVideo!.id!))
          });
        },
      ),
    );

    if (state is FacebookLiveProcessing) {
      items.add(SizedBox(height: 50,));
      items.add(LoadingWidget());
    } else if (product == null) {
      items.add(SizedBox(height: 50,));
      items.add(Text('ไม่พบสินค้า', style: Theme.of(context).textTheme.headline5, textAlign: TextAlign.center,));
    } else if (product != null) {
      items.add(SizedBox(height: 20,));
      Item? item = product!.defaultItem();
      if (item != null) {
        int price = item.price!;
        int? discountPrice = item.discountPrice;
        items.add(Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            ProductDefaultImageWithShowLiveOverlay(
              product: product!,
              item: null,
              onShowOverlay: (productImage) {
                context.read<FacebookLiveBloc>().add(ShowImageObs(
                  facebookLiveVideo: facebookLiveVideo!,
                  product: product!,
                  productImage: productImage
                ));
              },
              onHideOverlay: () {
                context.read<FacebookLiveBloc>().add(HideImageObs(
                  facebookLiveVideo: facebookLiveVideo!,
                  product: product!,
                ));
              },
            ),
            SizedBox(width: 8,),
            Expanded(child:  Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                RefNoBox(refNo: product!.refNo!,),
                Text(product!.title!),
                SizedBox(height: 8,),
                (discountPrice == null || price == discountPrice) ?
                  Text('ราคา $price บาท')
                  :
                  Row(
                    children: [
                      Text('ราคา '),
                      Text(price.toString(), style: TextStyle(decoration: TextDecoration.lineThrough)),
                      Text(' $discountPrice บาท')
                    ],
                  )

              ],
            )),
          ],
        ));
      }

      addVariantBox(context, items);
      items.add(SizedBox(height: 8,));
      items.add(Text('Description', style: TextStyle(fontWeight: FontWeight.bold),));

      if (product!.description != null) {
        items.add(Text(product!.description!));
      }
      items.add(SizedBox(height: 8,));
      items.add(Text('Specification', style: TextStyle(fontWeight: FontWeight.bold),));
      if (product!.specification != null) {
        items.add(Text(product!.specification!));
      }
      items.add(SizedBox(height: 8,));
      List<Widget> columns2 = [];
      for (int i = 0; i < product!.tagNames!.length; i++) {
        columns2.add(RefNoBox(refNo: product!.tagNames![i]));
        columns2.add(SizedBox(width: 8,));
      }
      items.add(Row(
        children: columns2
      ));
    }

    return RefreshIndicator(
      child: Column(
        children: [
          Expanded(
            child: ListView(
              padding: EdgeInsets.all(16.w),
              children: items,
              physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
            )
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              productBottomButton(context, state)
            ],
          )
        ],
      ),
      onRefresh: () async {
        _completer = Completer();
        context.read<FacebookLiveBloc>().add(SearchProduct(refno: product!.refNo!, facebookVideoId: facebookLiveVideo!.id!));
        return _completer!.future;
      },
    );
  }

  Widget productBottomButton(BuildContext context, FacebookLiveState state) {
    if (sellProduct == null && product == null) {
      return Container();
    }

    if (sellProduct != product) {
      return ElevatedButton(
        onPressed:  (state is FacebookLiveProcessingBackGround) ? null : () {
          if (product != null) {
            context.read<FacebookLiveBloc>().add(SellThisProduct(facebookLiveVideo: facebookLiveVideo!, product: product!));
          }
        },
        child: Text('Sell This Product')
      );
    }

    if (hideOverlay == false) {
      return ElevatedButton(
        onPressed: () {
          if (product != null) {
            context.read<FacebookLiveBloc>().add(HideLiveObs());
          }
        },
        child: Text('Hide Overlay')
      );
    }

    return ElevatedButton(
      onPressed: () {
        if (product != null) {
          context.read<FacebookLiveBloc>().add(DisplayLiveObs(productId: product!.id!, facebookVideoId: facebookLiveVideo!.id!));
        }
      },
      child: Text('Show Overlay')
    );
  }

  void addVariantBox(BuildContext contxt, List<Widget> items) {
    items.add(SizedBox(height: 8,));
    items.add(Text('Note', style: TextStyle(fontWeight: FontWeight.bold),));
    items.add(SizedBox(height: 8,));
    if (product!.note != null && product!.note!.isNotEmpty) {
      items.add(Text(product!.note!));
      items.add(SizedBox(height: 8,));
    }

    Item? item = product!.defaultItem();
    if (product!.hasOptions! && item != null && item.bin!.length > 0) {
      items.add(Text('Default bin', style: TextStyle(fontWeight: FontWeight.bold),));
      items.add(SizedBox(height: 8,));
      items.add(rowBins(context, item.bin));
      items.add(SizedBox(height: 8,));
    }

    List<TableRow> itemRows = [];

    itemRows.add(
      TableRow(
        children: [
          Text('Variants', style: TextStyle(fontWeight: FontWeight.bold),),
          Text('Quantity', style: TextStyle(fontWeight: FontWeight.bold),),
          Text('Available', style: TextStyle(fontWeight: FontWeight.bold),),
          Text('Bins', style: TextStyle(fontWeight: FontWeight.bold),),
        ]
      )
    );

    if (product!.hasOptions!) {
      for (var i = 0; i < product!.items!.length; i++) {
        Item item = product!.items![i];
        if (item.type == 'variation') {
          List<Widget> variationItems = [];

          for (var j = 0; j < item.itemOptions!.length; j++) {
            String itemOption = item.itemOptions![j];
            variationItems.add(
              Container(
                child: Text(
                  itemOption.split("|")[1],
                ),
                decoration: BoxDecoration(
                  color: Color.fromRGBO(235,235,235,1.0),
                  borderRadius: BorderRadius.circular(5.0),
                ),
                padding: EdgeInsets.all(5),
              ),
            );
            variationItems.add(
              SizedBox(width: 5),
            );
          }

          Widget variations = Row(
            children: variationItems,
          );

          Widget quantity = Text(" ${item.quantity} ");
          Widget available = Text(" ${item.available} ");

          Widget bins = rowBins(context, item.bin);

          itemRows.add(
            TableRow(
              children: [
                variations,
                quantity,
                available,
                bins,
              ]
            )
          );
        }
      }
    } else {

      Item? item = product!.defaultItem();
      Widget variations = Text('Default');
      Widget quantity = Container();
      Widget available = Container();
      Widget bins = Container();
      if (item != null) {
        quantity = Text(" ${item.quantity} ");
        available = Text(" ${item.available} ");
        bins = rowBins(context, item.bin);
      }
      itemRows.add(
        TableRow(
          children: [
            variations,
            quantity,
            available,
            bins,
          ]
        )
      );
    }

    items.add(Table(
      defaultVerticalAlignment: TableCellVerticalAlignment.middle,
      children: itemRows,
    ));
  }

  Widget rowBins(BuildContext context, List<String>? bins) {
    if (bins == null) {
      return Container();
    }

    List<Widget> items = [];

    for (int i = 0; i < bins.length; i++) {
      items.add(binBox(context, bins[i]));
      items.add(SizedBox(width: 5));
    }

    return Row(children: items,);
  }

  Widget binBox(BuildContext context, String bin) {
    return Container(
      child: Text(
        bin,
      ),
      decoration: BoxDecoration(
        color: Color.fromRGBO(240, 240, 80, 1),
        borderRadius: BorderRadius.circular(5.0),
      ),
      padding: EdgeInsets.all(5),
    );
  }



  Widget liveTap(BuildContext context, FacebookLiveState state) {
    List<Widget> items = [];
    if (facebookLiveFeeds != null && facebookLiveFeeds!.isNotEmpty) {
      for (int i = 0; i < facebookLiveFeeds!.length; i++) {
        items.add(feedRow(context, facebookLiveFeeds![i]));
      }
    }

    if (state is FacebookLiveProcessing) {
      items.add(LoadingWidget());
    }


    return RefreshIndicator(
      child: ListView(
        controller: _scrollController,
        children: items,
        physics: const BouncingScrollPhysics(parent: AlwaysScrollableScrollPhysics()),
      ),
      onRefresh: () async {
        _completer = Completer();
        context.read<FacebookLiveBloc>().add(LiveInit());
        return _completer!.future;
      },
    );
  }

  Widget feedRow(BuildContext context, FacebookLiveFeed facebookLiveFeed) {
    final formatDay = DateFormat('d/M/yy');
    final formatTime = DateFormat('HH:mm:ss');

    Color bgColor = Colors.transparent;

    if (facebookLiveFeed.successReply == true) {
      bgColor = Color(0Xffeeffdd);
    } else if (facebookLiveFeed.successReply == false) {
      bgColor = Color(0Xffffddcc);
    }

    String verb = (facebookLiveFeed.verb != 'add') ? ' (${facebookLiveFeed.verb})' : '';

    return Container(
      padding: EdgeInsets.symmetric(vertical: 4, horizontal: 8),
      decoration: BoxDecoration(
        color: bgColor,
        border: Border(
          bottom: BorderSide(width: 1.0, color: Colors.grey.shade300),
        )
      ),
      child: Row(
        children: [
          Container(
            width: 50.0,
            height: 50.0,
            child: CachedNetworkImage(
              imageUrl: facebookLiveFeed.image!,
              placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
              errorWidget: (context, url, error) => Icon(Icons.error),
            ),
          ),
          SizedBox(width: 8,),
          Expanded(child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(facebookLiveFeed.name!, style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Theme.of(context).primaryColor),),
              (facebookLiveFeed.parent != null) ?
                Text('Reply to ${facebookLiveFeed.parent!.name}', style: Theme.of(context).textTheme.overline,)
                :
                Container(),
              Text(facebookLiveFeed.message! + verb),
              (facebookLiveFeed.media != null && facebookLiveFeed.media != '') ?
                Container(
                  width: 50.0,
                  height: 50.0,
                  child: CachedNetworkImage(
                    imageUrl: facebookLiveFeed.media!,
                    placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                )
                :
                Container()
            ],
          )),
          SizedBox(width: 8,),
          Column(
            children: [
              Text(formatDay.format(facebookLiveFeed.createdAt!.toLocal())),
              Text(formatTime.format(facebookLiveFeed.createdAt!.toLocal())),
              (facebookLiveFeed.broadCastTime == null) ?
                Container()
                :
                Text('(${Duration(seconds: facebookLiveFeed.broadCastTime!).toString().substring(0, 7)})')
            ],
          )

        ],
      ),
    );
  }
}