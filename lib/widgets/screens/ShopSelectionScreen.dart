import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:quiver/strings.dart';

import 'package:shophil_live/bloc/authLayout/AuthLayoutBloc.dart';

import '../../bloc/authenticate/Authenticate.dart';
import '../../bloc/shopSelection/ShopSelection.dart';
import '../../repositories/ShopRepository.dart';
import '../../models/Shop.dart';

class ShopSelectionScreen extends StatefulWidget {
  ShopSelectionScreen({Key? key}) : super(key: key);

  @override
  State<ShopSelectionScreen> createState() => _ShopSelectionScreenState();
}

class _ShopSelectionScreenState extends State<ShopSelectionScreen> {
  List<Shop> shops = [];

  @override
  void initState() {
    shops = [];
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Select shops'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            onPressed: () {
              context.read<AuthenticationBloc>().add(LoggedOut());
            },
          ),
        ]
      ),
      body: BlocProvider(
        create: (BuildContext context) {
          return ShopSelectionBloc(
            authLayoutBloc: context.read<AuthLayoutBloc>(),
            authenticationBloc: context.read<AuthenticationBloc>(),
            shopRepository: context.read<ShopRepository>()
          )..add(Start());
        },
        child: BlocConsumer<ShopSelectionBloc, ShopSelectionState>(
          listener: _mainContentListenter,
          builder: _mainContentBuilder,
        ),
      ),
    );
  }

  void _mainContentListenter(BuildContext context, ShopSelectionState state) async {
    if (state is ShopsLoaded) {
      shops = state.shops;
    }
  }

  selectShop(BuildContext context, Shop shop) {
    context.read<ShopSelectionBloc>().add(SelectShop(shop: shop));
  }

  Widget _mainContentBuilder(BuildContext context, ShopSelectionState state) {
    return ListView.builder(
      itemBuilder: (BuildContext context, int index) {
        Shop shop = shops[index];
        return InkWell(
          onTap: () => selectShop(context, shop),
          child:  Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
            child: Container(
              child: Row(
                children: <Widget>[
                  Container(
                    width: 100.0,
                    height: 100.0,
                    child: CachedNetworkImage(
                      imageUrl: shop.logoMediumPath!,
                      placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
                      errorWidget: (context, url, error) => Icon(Icons.error),
                    ),
                  ),
                  Expanded(
                    child: Container(
                      padding: EdgeInsets.all(15),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Text(shop.name!,
                              style: Theme.of(context).textTheme.headline5),
                          Text( isBlank(shop.description)? '' : shop.description!),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          )
        );
      },
      itemCount: shops.length,
    );
  }
}