import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';

import '../../bloc/loginMain/LoginMain.dart';
import '../../bloc/authenticate/AuthenticationBloc.dart';
import '../../managers/AuthenticationManager.dart';
import '../../repositories/UserRepository.dart';

import '../common/DefaulTextInput.dart';
import '../common/PasswordTextField.dart';
import '../common/PrimaryRaiseButton.dart';
import '../common/SecondaryActionButton.dart';

class LoginMainScreen extends StatefulWidget {
  LoginMainScreen({ Key? key }) : super(key: key);

  @override
  _LoginMainScreenState createState() => _LoginMainScreenState();

}

class _LoginMainScreenState extends State<LoginMainScreen> {
  final _formKey = GlobalKey<FormState>();
  String _password = '';
  String _username = '';

  _onLoginButtonPressed(BuildContext context) {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      context.read<LoginMainBloc>().add(LoginButtonPressed(username: _username.trim(), password: _password.trim()));
    }
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider<LoginMainBloc>(
      create: (BuildContext context) {
        return LoginMainBloc(
          authenticationBloc: context.read<AuthenticationBloc>(),
          authenticationManager: context.read<AuthenticationManager>(),
          userRepository: context.read<UserRepository>()
        );
      },
      child: BlocConsumer<LoginMainBloc, LoginMainState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder,
      ),
    );
  }

  void _mainContentListenter(BuildContext context, LoginMainState state) async {

  }

  Widget _mainContentBuilder(BuildContext context, LoginMainState state) {
    return Scaffold(
      body: Container(
        width: 1.sw,
        decoration: BoxDecoration(
          gradient: LinearGradient(
            begin: Alignment.bottomLeft,
            end: Alignment.topRight,
            stops: [0.25, 0.75],
            colors: [
              Color.fromRGBO(34,118,173,1.0),
              Color.fromRGBO(110,198,84,1.0),
            ],
          )
        ),
        alignment: Alignment.center,
        child: SingleChildScrollView(
          padding: EdgeInsets.only(left: 24.w, right: 24.w),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 150,
                  child: Center(
                    child: Image(
                      image: AssetImage("assets/shophil-logo.png"),
                    ),
                  )
                ),
                SizedBox(height: 20,),
                DefaultTextInput(
                key: Key('userInput'),
                label:  'Email',
                validator: (value) {
                  if (value == null) {
                    return null;
                  }

                  if (value.isEmpty) {
                    return 'เกิดข้อผิดพลาด';
                  }
                  return null;
                },
                onSaved: (val) => _username = val ?? '',
              ),
              SizedBox(height: 20,),
              PasswordTextField(
                key: Key('passwordInput'),
                onSaved: (val) => _password = val ?? '',
                label: 'Password',
                validator: (value) {
                  if (value == null) {
                    return null;
                  }

                  if (value.isEmpty) {
                    return 'เกิดข้อผิดพลาด';
                  }
                  return null;
                },
              ),
              SizedBox(height: 48,),
              SizedBox(
                width: double.infinity,
                child: PrimaryRaiseButton(
                  onPressed: (state is! LoginMainLoading) ? () {
                    _onLoginButtonPressed(context);
                  } : null,
                  text: 'Sign in',
                  isProcessing: (state is LoginMainLoading),
                ),
              ),
              SizedBox(height: 24,),
              Row(
                children: <Widget>[
                  Expanded(child: Container(
                    height: 1,
                    color: Colors.white,
                  )),
                  SizedBox(width: 12,),
                  Text('Or', style: Theme.of(context).textTheme.bodyText2!.copyWith(color: Colors.white),),
                  SizedBox(width: 12,),
                  Expanded(child: Container(
                    height: 1,
                    color: Colors.white,
                  )),
                ],
              ),
              SizedBox(height: 24,),
              SecondaryActionButton(
                outlineColor: Theme.of(context).primaryColor,
                onPressed: () {
                  // BlocProvider.of<LoginBloc>(context).add(FacebookButtonPressed());
                  context.read<LoginMainBloc>().add(FacebookButtonPressed());
                },
                //child: Text(Translation.trans('mobile.auth.sign_in_with_facebook')),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 1.sw - 48.w,
                      padding: EdgeInsets.only(left: 30, right: 30),
                      child: Row(
                        children: <Widget>[
                          SvgPicture.asset('assets/fb-icon.svg'),
                          SizedBox(width: 16,),
                          Expanded(
                            child: Text('Login with facebook', style: Theme.of(context).textTheme.headline6!.copyWith(color: Colors.black)),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              )
            ],
            ),
          )
        ),
      ),
    );
  }
}