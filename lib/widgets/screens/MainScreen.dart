import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/main/Main.dart';

import './ProfileScreen.dart';
import './LiveScreen.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return BlocProvider<MainBloc>(
      create: (BuildContext context){
        return MainBloc();
      },
      child: BlocConsumer<MainBloc, MainState>(
        listener: _mainContentListenter,
        builder: _mainContentBuilder,
      ),
    );
  }

  void _mainContentListenter(BuildContext context, MainState state) async {
    if (state is TabChanged) {
      index = state.index;
    }
  }

  Widget _mainContentBuilder(BuildContext context, MainState state) {
    if (index == 0) {
      // Show Live screen
      return LiveScreen();
    } else if (index == 1) {
      // Show Profile screen
      return ProfileScreen();
    }

    return Container();
  }
}