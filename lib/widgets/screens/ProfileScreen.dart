import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:package_info/package_info.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../bloc/authLayout/AuthLayoutBloc.dart';
import '../../bloc/authLayout/AuthLayoutEvent.dart';
import '../../bloc/authenticate/AuthenticationBloc.dart';
import '../../bloc/authenticate/AuthenticationEvent.dart';

import '../common/BottomNavigation.dart';

class ProfileScreen extends StatefulWidget {
  ProfileScreen({Key? key}) : super(key: key);

  @override
  State<ProfileScreen> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String packageName = '';

  @override
  void initState() {

    PackageInfo.fromPlatform().then((PackageInfo packageInfo) {
      String appName = packageInfo.appName;
      String version = packageInfo.version;
      String buildNumber = packageInfo.buildNumber;

      setState(() {
        packageName = '$appName $version($buildNumber)';
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Center(
        child: ListView(
          children: <Widget>[
            SizedBox(height: 50,),
            Container(
              child: CachedNetworkImage(
                imageUrl: context.read<AuthLayoutBloc>().shop!.logoMediumPath!,
                placeholder: (context, url) => Image(image: AssetImage('assets/placeholder150x150.png')),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
              height: 150,
            ),
            SizedBox(height: 10,),
            Center(
              child: Text(
                context.read<AuthLayoutBloc>().shop!.name!,
                style: DefaultTextStyle.of(context).style.apply(fontSizeFactor: 0.45,color: Colors.black),
              ),
            ),
            SizedBox(height: 10,),
            Center(
              child: Text(
                "(${context.read<AuthenticationBloc>().sessionUser!.email})"
              ),
            ),
            SizedBox(height: 20,),
            Center(
              child: ElevatedButton(
                onPressed: (){
                  context.read<AuthLayoutBloc>().add(ChangeToSelectShop());
                },
                child: const Text('Switch shop'),
              ),
            ),
            SizedBox(height: 10,),
            Center(
              child: ElevatedButton(
                onPressed: (){
                  context.read<AuthenticationBloc>().add(LoggedOut());
                },
                child: const Text('Sign out'),
              ),
            ),
            SizedBox(height: 30,),
            (packageName != '') ?
              Text(packageName, textAlign: TextAlign.center,)
              :
              Container()
          ]
        )
      ),
      bottomNavigationBar: BottomNavigation(currentIndex: 1,),
    );
  }
}