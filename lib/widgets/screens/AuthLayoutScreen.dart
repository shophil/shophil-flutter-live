import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../bloc/authLayout/AuthLayout.dart';
import '../../repositories/ShopRepository.dart';

import './MainScreen.dart';
import './ShopSelectionScreen.dart';

class AuthLayoutScreen extends StatefulWidget {
  AuthLayoutScreen({Key? key}) : super(key: key);

  @override
  _AuthLayoutScreenState createState() => _AuthLayoutScreenState();
}

class _AuthLayoutScreenState extends State<AuthLayoutScreen> {

  @override
  Widget build(BuildContext context) {
    return BlocProvider<AuthLayoutBloc>(
      create: (BuildContext context){
        return AuthLayoutBloc(shopRepository: context.read<ShopRepository>())..add(HomeStart());
      },
      child: BlocBuilder<AuthLayoutBloc, AuthLayoutState>(
        builder: (BuildContext context, AuthLayoutState state) {
          if (state is SelectShop) {
            return ShopSelectionScreen();
          }

          if (state is Dashboard){
            return MainScreen();
          }
          return Container();
        },
      ),
    );
  }
}