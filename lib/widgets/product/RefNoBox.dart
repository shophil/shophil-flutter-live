import 'package:flutter/material.dart';

class RefNoBox extends StatelessWidget {
  final String refNo;

  const RefNoBox({Key? key, required this.refNo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        refNo,
        style: TextStyle(color: Colors.white),
      ),
      decoration: BoxDecoration(
        color: Color(0XFF659be0),
        borderRadius: BorderRadius.circular(5.0),
      ),
      padding: EdgeInsets.all(5),
    );
  }
}