import 'package:flutter/material.dart';

class BinBox extends StatelessWidget {
  final String bin;

  const BinBox({Key? key, required this.bin}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Text(
        bin,
        style: TextStyle(color: Colors.white),
      ),
      decoration: BoxDecoration(
        color: Color(0XFFe87e04),
        borderRadius: BorderRadius.circular(5.0),
      ),
      padding: EdgeInsets.all(5),
    );
  }
}