import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../../models/Product.dart';
import '../../models/ProductImage.dart';
import '../../models/Item.dart';

import './ProductImageObs.dart';

class ProductDefaultImageWithShowLiveOverlay extends StatefulWidget {
  final Product product;
  final Item? item;
  final void Function(ProductImage productImage) onShowOverlay;
  final void Function() onHideOverlay;

  ProductDefaultImageWithShowLiveOverlay({Key? key, required this.product, required this.item, required this.onShowOverlay, required this.onHideOverlay}) : super(key: key);

  @override
  State<ProductDefaultImageWithShowLiveOverlay> createState() => _ProductDefaultImageWithShowLiveOverlayState();
}

class _ProductDefaultImageWithShowLiveOverlayState extends State<ProductDefaultImageWithShowLiveOverlay> {
  String? imageId;

  @override
  Widget build(BuildContext context) {
    String url = 'https://shophil-web.s3.ap-southeast-1.amazonaws.com/assets/photo_default_square.png';
    if(widget.product.productImages != null && widget.product.productImages!.length > 0){
      url = widget.product.productImages![0].imageSquarePath!;
      if (widget.item != null && widget.item!.imageIds != null && widget.item!.imageIds!.isNotEmpty) {
        ProductImage? itemImage = widget.product.productImages!.firstWhereOrNull((element) => element.id == widget.item!.imageIds!.first);
        if (itemImage != null) {
          url = itemImage.imageSquarePath!;
        }
      }

      return InkWell(
        onTap: () => onTap(context),
        child: Container(
          height: 100,
          width: 100,
          child: CachedNetworkImage(
            imageUrl: url,
            placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        )
      );
    }

    return Container(
      height: 100,
      width: 100,
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );


  }

  void onTap(BuildContext context){
    showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
      double width = MediaQuery.of(context).size.width;
      if(widget.product.productImages!.length == 0){
        return CarouselSlider(
          options: CarouselOptions(
            height:  MediaQuery.of(context).size.height*40/100,
            initialPage: 0,
            autoPlay: false,
            enlargeCenterPage: true,
          ),
          items: [
            Container(
              width: width,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: CachedNetworkImage(
                imageUrl: 'https://shophil-web.s3.ap-southeast-1.amazonaws.com/assets/photo_default_square.png',
                placeholder: (context, url) => Image(image: AssetImage('assets/placeholder600x600.png')),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            )
          ],
        );
      }

      List<ProductImage> productImages = widget.product.productImages!;

      if (widget.item != null && widget.item!.imageIds != null && widget.item!.imageIds!.isNotEmpty) {
        List<ProductImage> itemImages = [];
        for (int i = 0; i < widget.item!.imageIds!.length; i++) {
          ProductImage? itemImage = widget.product.productImages!.firstWhereOrNull((element) => widget.item!.imageIds![i] == element.id);
          if (itemImage != null) {
             itemImages.add(itemImage);
          }
        }
        productImages = itemImages;
      }

      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: CarouselSlider.builder(
          options: CarouselOptions(
            height:  MediaQuery.of(context).size.height,
            aspectRatio: 16/9,
            viewportFraction: 0.8,
            initialPage: 0,
            reverse: false,
            autoPlay: false,
            autoPlayCurve: Curves.fastOutSlowIn,
            pauseAutoPlayOnTouch: true,
            enlargeCenterPage: true,
          ),
          itemCount: productImages.length - 1,
          itemBuilder: (BuildContext context, int itemIndex, int pageViewIndex) {
            final item = productImages[itemIndex];
            return ProductImageObs(
              onHideOverlay: widget.onHideOverlay,
              onShowOverlay: widget.onShowOverlay,
              productImage: item,
            );
          }
        )
      );
    });
  }
}