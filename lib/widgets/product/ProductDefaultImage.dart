import 'package:flutter/material.dart';
import 'package:collection/collection.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';

import '../../models/Product.dart';
import '../../models/ProductImage.dart';
import '../../models/Item.dart';

class ProductDefaultImage extends StatelessWidget {
  final Product product;
  final Item? item;

  const ProductDefaultImage({Key? key, required this.product, required this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    String url = 'https://shophil-web.s3.ap-southeast-1.amazonaws.com/assets/photo_default_square.png';
    if(product.productImages != null && product.productImages!.length > 0){
      url = product.productImages![0].imageSquarePath!;
      if (item != null && item!.imageIds != null && item!.imageIds!.isNotEmpty) {
        ProductImage? itemImage = product.productImages!.firstWhereOrNull((element) => element.id == item!.imageIds!.first);
        if (itemImage != null) {
          url = itemImage.imageSquarePath!;
        }
      }

      return InkWell(
        onTap: () => onTap(context),
        child: Container(
          height: 100,
          width: 100,
          child: CachedNetworkImage(
            imageUrl: url,
            placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        )
      );
    }

    return Container(
      height: 100,
      width: 100,
      child: CachedNetworkImage(
        imageUrl: url,
        placeholder: (context, url) => Image(image: AssetImage('assets/placeholder100x100.png')),
        errorWidget: (context, url, error) => Icon(Icons.error),
      ),
    );


  }

  void onTap(BuildContext context){
    showModalBottomSheet<void>(context: context, builder: (BuildContext context) {
      double width = MediaQuery.of(context).size.width;
      if(product.productImages!.length == 0){
        return CarouselSlider(
          options: CarouselOptions(
            height:  MediaQuery.of(context).size.height*40/100,
            initialPage: 0,
            enlargeCenterPage: true,
          ),
          items: [
            Container(
              width: width,
              margin: EdgeInsets.symmetric(horizontal: 5.0),
              child: CachedNetworkImage(
                imageUrl: 'https://shophil-web.s3.ap-southeast-1.amazonaws.com/assets/photo_default_square.png',
                placeholder: (context, url) => Image(image: AssetImage('assets/placeholder600x600.png')),
                errorWidget: (context, url, error) => Icon(Icons.error),
              ),
            )
          ],
        );
      }

      List<ProductImage> productImages = product.productImages!;

      if (item != null && item!.imageIds != null && item!.imageIds!.isNotEmpty) {
        List<ProductImage> itemImages = [];
        for (int i = 0; i < item!.imageIds!.length; i++) {
          ProductImage? itemImage = product.productImages!.firstWhereOrNull((element) => item!.imageIds![i] == element.id);
          if (itemImage != null) {
             itemImages.add(itemImage);
          }
        }
        productImages = itemImages;
      }

      return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: CarouselSlider(
          options: CarouselOptions(
            height:  MediaQuery.of(context).size.height,
            aspectRatio: 16/9,
            viewportFraction: 0.8,
            initialPage: 0,
            reverse: false,
            autoPlay: true,
            autoPlayInterval: Duration(seconds: 3),
            autoPlayAnimationDuration: Duration(milliseconds: 800),
            autoPlayCurve: Curves.fastOutSlowIn,
            pauseAutoPlayOnTouch: true,
            enlargeCenterPage: true,
          ),
          items: productImages.map((item){
            return Builder(
              builder: (BuildContext context) {
                return Container(
                  width: width,
                  margin: EdgeInsets.symmetric(horizontal: 5.0),
                  child: CachedNetworkImage(
                    imageUrl: item.imageLargePath!,
                    placeholder: (context, url) => Image(image: AssetImage('assets/placeholder600x600.png')),
                    errorWidget: (context, url, error) => Icon(Icons.error),
                  ),
                );
              }
            );
          }).toList(),
        )
      );
    });
  }
}