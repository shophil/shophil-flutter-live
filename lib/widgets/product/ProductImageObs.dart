import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

import '../../models/ProductImage.dart';

class ProductImageObs extends StatefulWidget {
  final ProductImage productImage;
  final void Function(ProductImage productImage) onShowOverlay;
  final void Function() onHideOverlay;

  ProductImageObs({Key? key, required this.productImage, required this.onShowOverlay, required this.onHideOverlay}) : super(key: key);

  @override
  State<ProductImageObs> createState() => _ProductImageObsState();
}

class _ProductImageObsState extends State<ProductImageObs> {

  bool isShow = false;

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Column(
      children: [
        (isShow) ?
        ElevatedButton(
          onPressed: () {
            widget.onHideOverlay();
            setState(() {
              isShow = false;
            });
          },
          child: Text('Hide Overlay')
        ) :
        ElevatedButton(
          onPressed: () {
            widget.onShowOverlay(widget.productImage);
            setState(() {
              isShow = true;
            });
          },
          child: Text('Show Overlay')
        ),
        Expanded(child: Container(
          width: width,
          margin: EdgeInsets.symmetric(horizontal: 5.0),
          child: CachedNetworkImage(
            imageUrl: widget.productImage.imageLargePath!,
            placeholder: (context, url) => Image(image: AssetImage('assets/placeholder600x600.png')),
            errorWidget: (context, url, error) => Icon(Icons.error),
          ),
        ))
      ],
    );

  }
}