import 'package:equatable/equatable.dart';

class ProductImage extends Equatable {
  final String? id;
  final int? width;
  final int? height;
  final String? imageOriginalPath;
  final String? imageThumbPath;
  final String? imageMediumPath;
  final String? imageLargePath;
  final String? imageSquarePath;
  final String? imageMinPath;

  ProductImage({this.id, this.width, this.height, this.imageOriginalPath, this.imageThumbPath, this.imageMediumPath, this.imageLargePath, this.imageSquarePath, this.imageMinPath});

  @override
  List<Object> get props => [id ?? 0];

  factory ProductImage.fromJson(Map<String, dynamic> json){
    ProductImage productImage =  ProductImage(
      id: json['id'],
      width: json['width'],
      height: json['height'],
      imageOriginalPath: json['image_original_path'],
      imageThumbPath: json['image_thumb_path'],
      imageMediumPath: json['image_medium_path'],
      imageLargePath: json['image_large_path'],
      imageSquarePath: json['image_square_path'],
      imageMinPath: json['image_min_path'],
    );

    return productImage;
  }

  @override
  String toString() => 'Product image { id: $id }';
}