import 'package:equatable/equatable.dart';

class Item extends Equatable {
  final String? id;
  final String? type;
  final List<String>? itemOptions;
  final int? quantity;
  final int? available;
  final int? reserved;
  final int? fulfilled;
  final int? incomming;
  final int? price;
  final int? discountPrice;
  final List<String>? bin;
  final List<String>? imageIds;

  Item({this.id, this.type, this.itemOptions, this.quantity, this.available, this.reserved, this.fulfilled, this.incomming, this.bin, this.price, this.discountPrice, this.imageIds});

  @override
  List<Object> get props => [id ?? 0];

  factory Item.fromJson(Map<String, dynamic> json) {
    // Channge List<dynamic> to List<String>
    List<String> options = [];
    if(json['item_options'] != null){
      for (var i = 0; i < json['item_options'].length; i++) {
        options.add(json['item_options'][i]);
      }
    }

    List<String> bin = [];
    if (json['bin'] != null) {
      for (var i = 0; i < json['bin'].length; i++) {
        bin.add(json['bin'][i]);
      }
    }

    List<String> imageIds = [];
    if (json['image_ids'] != null) {
      for (var i = 0; i < json['image_ids'].length; i++) {
        imageIds.add(json['image_ids'][i]);
      }
    }

    Item item = Item(
      id: json['id'],
      type: json['type'],
      itemOptions: options,
      quantity: json['quantity'],
      available: json['available'],
      reserved: json['reserved'],
      fulfilled: json['fulfilled'],
      incomming: json['incomming'],
      bin: bin,
      price: json['price'],
      discountPrice: json['discount_price'],
      imageIds: imageIds
    );
    return item;
  }
}