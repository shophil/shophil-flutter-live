import 'package:equatable/equatable.dart';


class FacebookLiveFeed extends Equatable {
  final String? id;
  final int? broadCastTime;
  final DateTime? createdAt;
  final String? image;
  final String? itemId;
  final String? media;
  final String? message;
  final String? name;
  final String? pageId;
  final String? postId;
  final String? psid;
  final bool? successReply;
  final String? verb;
  final FacebookLiveFeed? parent;

  FacebookLiveFeed({
    required this.id,
    required this.broadCastTime,
    required this.image,
    required this.createdAt,
    required this.itemId,
    required this.media,
    required this.message,
    required this.name,
    required this.pageId,
    required this.postId,
    required this.psid,
    required this.successReply,
    required this.verb,
    required this.parent
  });

  @override
  List<Object> get props => [id ?? 0];

  factory FacebookLiveFeed.fromJson(Map<String, dynamic> json){

    return FacebookLiveFeed(
      id: json['id'],
      broadCastTime: json['broad_cast_time'],
      createdAt: DateTime.parse(json['created_at']),
      image: json['image'],
      itemId: json['item_id'],
      media: json['media'],
      message: json['message'] ?? '',
      name: json['name'] ?? '',
      pageId: json['page_id'],
      postId: json['post_id'],
      psid: json['psid'],
      successReply: json['success_reply'],
      verb: json['verb'],
      parent: (json['parent'] != null) ?  FacebookLiveFeed.fromJson(json['parent']) : null
    );
  }

  @override
  String toString() => 'FacebookLiveFeed { id: $id }';
}