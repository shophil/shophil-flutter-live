import 'package:equatable/equatable.dart';

class FacebookLiveVideo extends Equatable {
  final String? id;
  final String? liveVideoId;
  final String? pageId;
  final String? status;
  final String? thumbnail;
  final String? title;
  final DateTime? updatedAt;

  FacebookLiveVideo({
    required this.id,
    required this.liveVideoId,
    required this.pageId,
    required this.status,
    required this.thumbnail,
    required this.title,
    required this.updatedAt
  });

  @override
  List<Object> get props => [id ?? 0];

  factory FacebookLiveVideo.fromJson(Map<String, dynamic> json){

    return FacebookLiveVideo(
      id: json['id'],
      liveVideoId: json['live_video_id'],
      pageId: json['page_id'],
      status: json['status'],
      thumbnail: json['thumbnail'],
      title: json['title'],
      updatedAt: DateTime.parse(json['updated_at']),
    );
  }

  @override
  String toString() => 'FacebookLiveVideo { id: $id }';
}