import 'package:equatable/equatable.dart';

import '../services/ShophilConfig.dart';

class Shop extends Equatable {
  final String? id;
  final String? name;
  final String? description;
  final String? logoFilename;
  final String? logoMediumPath;

  Shop({required this.id, required this.name, required this.logoMediumPath, required this.logoFilename, required this.description});

  @override
  List<Object> get props => [id ?? 0];

  factory Shop.fromJson(Map<String, dynamic> json){
    String path = json['logo_medium_path'];
    if(json['logo_filename'] ==null) {
      path = ShophilConfig.defaultPhoto;
    }

    return Shop(
      id: json['id'],
      name: json['name'],
      logoFilename: json['logo_filename'],
      logoMediumPath: path,
      description: json['description'],
    );
  }

  @override
  String toString() => 'Shop { id: $id }';
}