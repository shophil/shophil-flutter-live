import 'package:equatable/equatable.dart';

import '../models/ProductImage.dart';
import '../models/Item.dart';

class Product extends Equatable {
  final String? id;
  final String? title;
  final String? refNo;
  final String? description;
  final String? specification;
  final bool? hasOptions;
  final String? note;

  final List<ProductImage>? productImages;
  final List<Item>? items;
  final List<String>? tagNames;

  Product({this.id, this.title, this.refNo, this.description, this.specification, this.hasOptions, this.note, this.productImages, this.items, this.tagNames});

  @override
  List<Object> get props => [id ?? 0];

  factory Product.fromJson(Map<String, dynamic> json){
    Product product =  Product(
      id: json['id'],
      title: json['title'],
      refNo: json['ref_no'],
      description: json['description'],
      specification: json['specification'],
      hasOptions: json['has_options'] == true,
      note: json['note'],
      productImages: [],
      items: [],
      tagNames: []
    );

    if(json['product_images'] != null){
      print("found product -> product images");
      for (var i = 0; i < json['product_images'].length; i++) {
        Map<String, dynamic> productImageMap = json['product_images'][i];
        ProductImage productImage = ProductImage.fromJson(productImageMap);
        product.productImages!.add(productImage);
      }
      print("Total images ${product.productImages!.length}");
    }

    if(json['items'] != null){
      print("found product -> product items");
      for (var i = 0; i < json['items'].length; i++) {
        Map<String, dynamic> itemMap = json['items'][i];
        Item item = Item.fromJson(itemMap);
        product.items!.add(item);
      }
      print("Total images ${product.items!.length}");
    }

    if(json['tag_names'] != null){
      for (var i = 0; i < json['tag_names'].length; i++) {
        product.tagNames!.add(json['tag_names'][i]);
      }
      print("Total tags ${product.tagNames!.length}");
    }

    return product;
  }

  Item? defaultItem(){
    Item? defaultItem;
    if (items == null) {
      return null;
    }

    for(var i = 0; i < items!.length; i++){
      if(items![i].type == 'default'){
        defaultItem =items![i];
      }
    }
    return defaultItem;
  }

  @override
  String toString() => 'Product { id: $id }';
}