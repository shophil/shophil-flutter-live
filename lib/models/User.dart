import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String? id;
  final String? email;
  final String? gender;
  final String? imageMediumPath;
  final String? name;

  User(this.id, this.email, this.imageMediumPath, this.gender, this.name);

  @override
  List<Object> get props => [id ?? 0];

  User.fromJson(Map<String, dynamic> json)
    :id = json['id'],
     email = json['email'],
     gender = json['gender'],
     imageMediumPath =json['image_medium_path'],
     name =json['name'];
}