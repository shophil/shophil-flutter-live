import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/FacebookLiveFeed.dart';

class FacebookLiveFeedRepository {
  final ApiClient apiClient;

  FacebookLiveFeedRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<List<FacebookLiveFeed>> fetchFeed({required String shopId, required String postId, required String? minId, required DateTime? minCreatedAt }) async {
    List<FacebookLiveFeed> feeds = [];

    final String path = '/$shopId/facebook_live_feeds';
    Map<String, String> queryParameters = {
      'post_id': postId,
    };

    if (minId != null) {
      queryParameters['min_id'] = minId;
    }
    if (minCreatedAt != null) {
      queryParameters['min_created_at'] = minCreatedAt.toIso8601String();
    }

    Response response = await apiClient.get(path, queryParameters: queryParameters);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('Something went wrong');
    }

    Map feedMap = decodeResponse(response);

    for (var i = 0; i < feedMap['facebook_live_feeds'].length; i++) {
      Map<String, dynamic> shopMap = feedMap['facebook_live_feeds'][i];
      FacebookLiveFeed feed = FacebookLiveFeed.fromJson(shopMap);
      feeds.add(feed);
    }

    return feeds;
  }

}