import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';

class FacebookLiveVideoProductRepository {
  final ApiClient apiClient;

  FacebookLiveVideoProductRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<bool> isExists({required String shopId, required String facebookVideoId, required String productId}) async {
    Response response = await apiClient.get('/$shopId/facebook_live_video_products?product_id=$productId&facebook_live_video_id=$facebookVideoId');

    Map data = decodeResponse(response);

    return data['facebook_live_video_products'].length > 0;
  }

  Future<void> addLiveVideoProduct({required String shopId, required String facebookVideoId, required String productId}) async {
    Map<String, dynamic> data = {
      'facebook_live_video_product': {
        'product_id': productId,
        'facebook_live_video': facebookVideoId
      }
    };

    Response response = await apiClient.post('/$shopId/facebook_live_video_products', data);

    if (response.statusCode! >= 400) {
      throw('Something went wrong');
    }
  }
}