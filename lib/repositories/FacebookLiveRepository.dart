import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/FacebookLiveVideo.dart';

class FacebookLiveRepository {
  final ApiClient apiClient;

  FacebookLiveRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<List<FacebookLiveVideo>> fetchVideos({required String shopId}) async {
    List<FacebookLiveVideo> facebookLiveViodes = [];

    final String path = '/$shopId/facebook_live_videos';
    Response response = await apiClient.get(path);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('Something went wrong');
    }

    Map liveMap = decodeResponse(response);

    for (var i = 0; i < liveMap['facebook_live_videos'].length; i++) {
      Map<String, dynamic> shopMap = liveMap['facebook_live_videos'][i];
      FacebookLiveVideo facebookLiveVideo = FacebookLiveVideo.fromJson(shopMap);
      facebookLiveViodes.add(facebookLiveVideo);
    }

    return facebookLiveViodes;
  }

  Future<FacebookLiveVideo?> getLastVideo({required String shopId}) async {
    List<FacebookLiveVideo> facebookLiveViodes = await fetchVideos(shopId: shopId);

    if (facebookLiveViodes.isEmpty) {
      return null;
    }

    return facebookLiveViodes.first;
  }
}