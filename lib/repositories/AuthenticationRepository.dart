import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';

class AuthenticationRepository {
  final ApiClient apiClient;

  AuthenticationRepository({required this.apiClient});

  Future<dynamic> loginCheck({required String username, required String password}) async {
    final String path = '/oauth/token';

    dynamic body = {
      'username': username,
      'password': password,
      "grant_type": "password",
      "client_id": "2"
    };

    Response response = await apiClient.post(path, jsonEncode(body), auth: false);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('invalid login');
    }

    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<dynamic> facebookAuthenticate({
    required String accessToken,
  }) async {
    final String path = '/oauth/token';

    final body = {
      "grant_type": "facebook_token",
      'facebook_token': accessToken,
       "client_id": "2"
    };
    final response = await apiClient.post(path, jsonEncode(body), auth: false);

    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }
}