import 'dart:convert';
import 'package:dio/dio.dart';

import '../data/ApiClient.dart';
import '../models/User.dart';

class UserRepository {
  final ApiClient apiClient;

  UserRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<User> getCurrentUser() async {
    final String path = '/users/me';

    Response response = await apiClient.get(path);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('session invalid');
    }

    Map<String, dynamic> json = decodeResponse(response);
     return User.fromJson(json['user']);
  }
}