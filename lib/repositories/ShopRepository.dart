import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

import '../data/ApiClient.dart';
import '../models/Shop.dart';

class ShopRepository {
  final ApiClient apiClient;
  final FlutterSecureStorage storage;

  ShopRepository({required this.apiClient, required this.storage});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<bool> hasSelectedShop() async {
    String? shopId = await storage.read(key: 'shop_id');
    print("shop_id : $shopId");
    if(shopId != null){
      return true;
    }
    return false;
  }

  Future<String?> getSelectedShopId() async {
    return await storage.read(key: 'shop_id');
  }

  Future<void> selectShop(Shop shop) async{
    await storage.write(key: 'shop_id', value: shop.id);
  }

  Future<List<Shop>> fetchShops({required String userId}) async{
    List<Shop> shops = [];

    final String path = '/users/$userId/shops';
    Response response = await apiClient.get(path);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('session invalid');
    }

    Map shopsMap = decodeResponse(response);

    for (var i = 0; i < shopsMap['shops'].length; i++) {
      Map<String, dynamic> shopMap =shopsMap['shops'][i];
      Shop shop = Shop.fromJson(shopMap);
      shops.add(shop);
    }

    return shops;
  }

  Future<Shop> fetchShop({required String shopId}) async{

    final String path = "/shops/$shopId";
    Response response = await apiClient.get(path);

    if (response.statusCode != ApiClient.HTTP_OK) {
      await this.removeSelectShop();
      throw('session invalid');
    }

    Map shopMap = decodeResponse(response);
    Shop shop = Shop.fromJson(shopMap['shop']);
    return shop;
  }

  Future<void> removeSelectShop() async {
    await storage.write(key: 'shop_id', value: null);
  }
}