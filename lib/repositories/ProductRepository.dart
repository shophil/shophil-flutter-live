import 'dart:convert';
import 'package:dio/dio.dart';
import 'package:shophil_live/models/ProductImage.dart';

import '../data/ApiClient.dart';
import '../models/Product.dart';
import '../models/ProductImage.dart';

class ProductRepository {
  final ApiClient apiClient;

  ProductRepository({required this.apiClient});

  Map<String, dynamic> decodeResponse(Response response) {
    if (response.data is String) {
      return jsonDecode(response.data);
    }
    return response.data;
  }

  Future<List<ProductImage>> fetchProductImages({required String shopId, required String productId}) async {
    List<ProductImage> images = [];

    final String path = '/$shopId/products/$productId/product_images';
    Response response = await apiClient.get(path);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('Something went wrong');
    }

    Map imageMaps = decodeResponse(response);

    for (var i = 0; i < imageMaps['product_images'].length; i++) {
      Map<String, dynamic> map = imageMaps['product_images'][i];
      ProductImage image = ProductImage.fromJson(map);
      images.add(image);
    }

    return images;
  }

  Future<List<Product>> fetchProducts({required String shopId, required String refNo}) async {
    List<Product> products = [];

    final String path = '/$shopId/products';
    Map<String, String> queryParameters = {
      'include': 'items,tag_names',
      'ref_no': refNo,
      'per_page': '1',
      'page': '1'
    };
    Response response = await apiClient.get(path, queryParameters: queryParameters);

    if (response.statusCode != ApiClient.HTTP_OK) {
      throw('Something went wrong');
    }

    Map productMaps = decodeResponse(response);

    print('found total ${productMaps['products'].length}');

    for (var i = 0; i < productMaps['products'].length; i++) {
      Map<String, dynamic> map = productMaps['products'][i];
      Product product = Product.fromJson(map);
      products.add(product);
    }

    return products;
  }

  Future<void> displayLiveOverlay({required String shopId, required String productId, required String facebookVideoId}) async {
    final String path = '/$shopId/products/$productId/display-live-overlay/$facebookVideoId';
    await apiClient.get(path);
  }

  Future<void> displayLiveOverlay2({required String shopId, required String productId, required String facebookVideoId, String? pictureId}) async {
    final String path = '/$shopId/products/$productId/display-live-overlay/$facebookVideoId';
    await apiClient.post(path, jsonEncode({
      "config": {
        "display_option": true,
        "display_picture": pictureId != null,
        "picture_id": pictureId,
        "picture_width": 100
      }
    }));
  }
}