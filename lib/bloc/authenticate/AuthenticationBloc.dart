import 'dart:async';
import 'package:bloc/bloc.dart';

import '../../config.dart';
import '../../managers/AuthenticationManager.dart';
import '../../repositories/UserRepository.dart';
import '../../models/User.dart';
import './Authenticate.dart';

class AuthenticationBloc extends Bloc<AuthenticationEvent, AuthenticationState> {
  final AuthenticationManager authenticationManager;
  final UserRepository userRepository;

  User? sessionUser;

  AuthenticationBloc({required this.authenticationManager, required this.userRepository}) : super(AuthenticationUninitialized());

  @override
  Stream<AuthenticationState> mapEventToState(AuthenticationEvent event) async* {
    if (event is AppStarted) {
      yield* _mapAppStarted();
    }

    if (event is LoggedIn) {
      yield* _mapLoggedIn(event);
    }

    if (event is LoggedOut) {
      yield* _mapLoggedOut();
    }

    if (event is SetUser) {
      this.sessionUser = event.user;
    }
  }

  Stream<AuthenticationState> _mapAppStarted() async* {
    yield AuthenticationLoading();
      await Config().loadEnv();
    try {
      await authenticationManager.getToken();
      sessionUser = await userRepository.getCurrentUser();
      yield AuthenticationAuthenticated();
    } catch (e) {
      print(e.toString());
      print('unauthenticate');
      yield AuthenticationUnauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedIn(LoggedIn event) async* {
    yield AuthenticationLoading();
    try {
      User user =  await userRepository.getCurrentUser();
      sessionUser = user;
      yield AuthenticationAuthenticated();
    } catch (e) {
      print(e.toString());
      yield AuthenticationUnauthenticated();
    }
  }

  Stream<AuthenticationState> _mapLoggedOut() async* {
     yield AuthenticationLoading();
     await authenticationManager.deleteToken();
     yield AuthenticationUnauthenticated();
  }
}