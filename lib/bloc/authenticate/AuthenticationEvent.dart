import 'package:equatable/equatable.dart';

import '../../models/User.dart';

abstract class AuthenticationEvent extends Equatable {
  const AuthenticationEvent();

  @override
  List<Object> get props => [];
}

class AppStarted extends AuthenticationEvent {
  @override
  String toString() => 'AppStarted';
}

class LoggedIn extends AuthenticationEvent {
  final String token;

  const LoggedIn({required this.token});

  @override
  List<Object> get props => [];

  @override
  String toString() {
    return 'LoggedIn {token: $token}';
  }
}

class LoggedOut extends AuthenticationEvent {
  @override
  String toString() => 'LoggedOut';
}

class SetUser extends AuthenticationEvent {
  final User user;
  SetUser({required this.user});
}
