import 'package:equatable/equatable.dart';

import '../../models/Shop.dart';

abstract class ShopSelectionState extends Equatable {
  const ShopSelectionState();

  @override
  List<Object> get props => [];
}

class ShopsUninitialized extends ShopSelectionState {
  @override
  String toString() => 'ShopsUninitialized';
}

class ShopsError extends ShopSelectionState {
  @override
  String toString() => 'ShopsError';
}

class ShopsLoaded extends ShopSelectionState {
  final List<Shop> shops;

  ShopsLoaded({
    required this.shops,
  });

  ShopsLoaded copyWith({
    required List<Shop> shops,
  }) {
    return ShopsLoaded(
      shops: shops,
    );
  }

  @override
  String toString() => 'ShopsLoaded { shops: ${shops.length}}';
}