import 'dart:async';
import 'package:bloc/bloc.dart';

import './ShopSelection.dart';
import '../../repositories/ShopRepository.dart';
import '../../bloc/authLayout/AuthLayoutBloc.dart';
import '../../bloc/authLayout/AuthLayoutEvent.dart';
import '../../bloc/authenticate/AuthenticationBloc.dart';
import '../../models/Shop.dart';

class ShopSelectionBloc extends Bloc<ShopSelectionEvent, ShopSelectionState> {
  final ShopRepository shopRepository;
  final AuthLayoutBloc authLayoutBloc;
  final AuthenticationBloc authenticationBloc;

  ShopSelectionBloc({required this.shopRepository, required this.authLayoutBloc, required this.authenticationBloc}) : super(ShopsUninitialized());


  @override
  Stream<ShopSelectionState> mapEventToState(ShopSelectionEvent event) async* {
    if (event is Start) {
      yield* _mapStart(event);
    } else if (event is SelectShop) {
      await shopRepository.selectShop(event.shop);
      authLayoutBloc.add(ChangeToDashboard(shop: event.shop));
    }
  }

  Stream<ShopSelectionState> _mapStart(Start event) async* {
    final bool selectedShop = await shopRepository.hasSelectedShop();
    if(selectedShop){
      try {
        print('fetch shop');
        String? shopId = await shopRepository.getSelectedShopId();
        if (shopId == null) {
          throw('No shop id');
        }
        final Shop shop = await shopRepository.fetchShop(shopId: shopId);
        await shopRepository.selectShop(shop);
        authLayoutBloc.add(ChangeToDashboard(shop: shop));
      } catch (e) {
        print('fetchs shop');
        final List<Shop> shops = await shopRepository.fetchShops(userId: authenticationBloc.sessionUser!.id!);
        yield ShopsLoaded(shops: shops);
      }
    }else{
      print('Not select shop');
      final List<Shop> shops = await shopRepository.fetchShops(userId: authenticationBloc.sessionUser!.id!);
      yield ShopsLoaded(shops: shops);
    }
  }
}