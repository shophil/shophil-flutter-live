import 'package:equatable/equatable.dart';

import '../../models/Shop.dart';

abstract class ShopSelectionEvent extends Equatable {
  @override
  List<Object> get props => [];
}

class Start extends ShopSelectionEvent {
  @override
  String toString() => 'Start';
}

class Fetch extends ShopSelectionEvent {
  @override
  String toString() => 'Fetch';
}

class SelectShop extends ShopSelectionEvent{
  final Shop shop;

  SelectShop({
    required this.shop,
  });

  @override
  String toString() => 'SelectShop';
}