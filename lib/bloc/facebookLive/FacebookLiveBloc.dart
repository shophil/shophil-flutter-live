import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:socket_io_client/socket_io_client.dart' as IO;

import '../../bloc/authLayout/AuthLayoutBloc.dart';
import './FacebookLive.dart';
import '../../repositories/FacebookLiveRepository.dart';
import '../../repositories/FacebookLiveFeedRepository.dart';
import '../../repositories/ProductRepository.dart';
import '../../repositories/FacebookLiveVideoProductRepository.dart';
import '../../models/FacebookLiveVideo.dart';
import '../../models/FacebookLiveFeed.dart';
import '../../models/Product.dart';
import '../../models/Item.dart';

class FacebookLiveBloc extends Bloc<FacebookLiveEvent, FacebookLiveState> {
  final AuthLayoutBloc authLayoutBloc;
  final FacebookLiveRepository facebookLiveRepository;
  final FacebookLiveFeedRepository facebookLiveFeedRepository;
  final FacebookLiveVideoProductRepository facebookLiveVideoProductRepository;
  final ProductRepository productRepository;
  late IO.Socket socket;
  Product? selectProduct;
  FacebookLiveVideo? selectFacebookLiveVideo;

  FacebookLiveBloc({required this.authLayoutBloc, required this.facebookLiveRepository, required this.facebookLiveFeedRepository, required this.productRepository, required this.facebookLiveVideoProductRepository}) : super(FacebookLiveUninitialized());

  @override
  Stream<FacebookLiveState> mapEventToState(FacebookLiveEvent event) async* {
    print(event.toString());
    if (event is LiveInit) {
      yield* _mapLiveInit(event);
    } else if (event is FetchFeedsMore) {
      yield* _mapFetchFeedsMore(event);
    } else if (event is SocketInit) {
      yield* _mapSocketInit(event);
    } else if (event is SocketPostSubscribe) {
      yield* _mapSocketPostSubscribe(event);
    } else if (event is SocketDisconnect) {
      yield* _mapSocketDisconnect(event);
    } else if (event is SyncMessage) {
      yield* _mapSyncMessage(event);
    } else if (event is SearchProduct) {
      yield* _mapSearchProduct(event);
    } else if (event is DisplayLiveObs) {
      yield* _mapDisplayLiveObs(event);
    } else if (event is HideLiveObs) {
      yield* _mapHideLiveObs(event);
    } else if (event is SyncItem) {
      yield* _mapSyncItem(event);
    } else if (event is SellThisProduct) {
      yield* _mapSellThisProduct(event);
    } else if (event is SocketFacebookVideoSubscribe) {
      yield* _mapSocketFacebookVideoSubscribe(event);
    } else if (event is ShowImageObs) {
      yield* _mapShowImageObs(event);
    } else if (event is HideImageObs) {
      yield* _mapHideImageObs(event);
    }
  }

  @override
  Future<void> close() async {
    print('socket disconnect');
    socket.disconnect();
    super.close();
  }

  Stream<FacebookLiveState> _mapShowImageObs(ShowImageObs event) async* {
    yield FacebookLiveProcessingBackGround();
    await this.productRepository.displayLiveOverlay2(
      shopId:  authLayoutBloc.shop!.id!,
      facebookVideoId: event.facebookLiveVideo.id!,
      productId: event.product.id!,
      pictureId: event.productImage.id
    );
  }

  Stream<FacebookLiveState> _mapHideImageObs(HideImageObs event) async* {
    yield FacebookLiveProcessingBackGround();
    await this.productRepository.displayLiveOverlay2(
      shopId:  authLayoutBloc.shop!.id!,
      facebookVideoId: event.facebookLiveVideo.id!,
      productId: event.product.id!,
      pictureId: null
    );
  }

  Stream<FacebookLiveState> _mapHideLiveObs(HideLiveObs event) async* {
    yield FacebookLiveProcessingBackGround();

    await this.productRepository.displayLiveOverlay(shopId: authLayoutBloc.shop!.id!, productId: '--impossible--', facebookVideoId: '--impossible--');

    yield HideShowUpdated(hideOverlay: true);
  }

  Stream<FacebookLiveState> _mapDisplayLiveObs(DisplayLiveObs event) async* {
    yield FacebookLiveProcessingBackGround();

    await this.productRepository.displayLiveOverlay(shopId: authLayoutBloc.shop!.id!, productId: event.productId, facebookVideoId: event.facebookVideoId);

    yield HideShowUpdated(hideOverlay: false);
  }

  Stream<FacebookLiveState> _mapSellThisProduct(SellThisProduct event) async* {
    yield FacebookLiveProcessingBackGround();

    try {
      await this.facebookLiveVideoProductRepository.addLiveVideoProduct(shopId: authLayoutBloc.shop!.id!, facebookVideoId: event.facebookLiveVideo.id!, productId: event.product.id!);
      yield SellProductLoaded(product: event.product);

      this.add(DisplayLiveObs(productId: event.product.id!, facebookVideoId: event.facebookLiveVideo.id!));

    } catch (e) {
      yield FailProcess(error: e.toString());
    }
  }

  Stream<FacebookLiveState> _mapSyncMessage(SyncMessage event) async* {
    yield FacebookLiveProcessingBackGround();

    try {
      Map<String, dynamic> json = event.data;
      if (json['id'] != null) {
        yield FacebookLiveFeedSync(facebookLiveFeed: FacebookLiveFeed.fromJson(json));
      } else if (json.length > 0) {
        for (int i = 0; i < json.length; i++) {
          yield FacebookLiveFeedSync(facebookLiveFeed: FacebookLiveFeed.fromJson(json[i]));
        }
      }
    } catch (e) {
      print('E002');
      print(e.toString());
    }
  }

  Stream<FacebookLiveState> _mapSyncItem(SyncItem event) async* {
    yield FacebookLiveProcessingBackGround();

    Map<String, dynamic> json = event.data;
    try {
      if (json['id'] != null) {
        yield ItemSync(item: Item.fromJson(json));
      }
    } catch (e) {
      print('E004');
      print(e.toString());
    }
  }

  Stream<FacebookLiveState> _mapSocketPostSubscribe(SocketPostSubscribe event) async* {
    final String fbLiveEvent = 'fb-live-${event.facebookLiveVideo.id}';
    final String fbLiveUpdateEvent = 'fb-live-update-${event.facebookLiveVideo.id}';
    // Just make sure we always off it
    print(fbLiveEvent);
    socket.off(fbLiveEvent);
    socket.off(fbLiveUpdateEvent);

    socket.on(fbLiveEvent, (data) {
      print('in1');
      print(data);
    });

    socket.on(fbLiveUpdateEvent, (data) {
      print('in2');
      print(data);
      add(SyncMessage(data: data));
    });
  }

  Stream<FacebookLiveState> _mapSocketFacebookVideoSubscribe(SocketFacebookVideoSubscribe event) async* {
    final String videoEvent = 'fb-video-${authLayoutBloc.shop!.id!}';

    socket.off(videoEvent);

    socket.on(videoEvent, (data) {
      print('video event trigger');
      if (selectFacebookLiveVideo != null && data['facebook_live_video']['id'] != selectFacebookLiveVideo!.id) {
        add(LiveInit());
      }
    });
  }

  Stream<FacebookLiveState> _mapSocketDisconnect(SocketDisconnect event) async* {
    socket.disconnect();
  }

  Stream<FacebookLiveState> _mapSocketInit(SocketInit event) async* {
    socket = IO.io('https://socket.shophil.com/', IO.OptionBuilder().setTransports(['websocket']).build());
    socket.onConnect((_) {
     print('connect');
    });

    socket.onConnectError((e) {
     print(e.toString());
    });
  }



  Stream<FacebookLiveState> _mapLiveInit(LiveInit event) async* {
    yield FacebookLiveProcessing();

    try {
      if (authLayoutBloc.shop == null) {
        return;
      }

      if (selectFacebookLiveVideo != null) {
        final String fbLiveEvent = 'fb-live-${selectFacebookLiveVideo!.id}';
        final String fbLiveUpdateEvent = 'fb-live-update-${selectFacebookLiveVideo!.id}';
        // Just make sure we always off it
        socket.off(fbLiveEvent);
        socket.off(fbLiveUpdateEvent);
      }


      FacebookLiveVideo? facebookLiveVideo = await facebookLiveRepository.getLastVideo(shopId: authLayoutBloc.shop!.id!);
      selectFacebookLiveVideo = facebookLiveVideo;

      List<FacebookLiveFeed> feeds = await facebookLiveFeedRepository.fetchFeed(shopId: authLayoutBloc.shop!.id!, postId: facebookLiveVideo!.id!, minId: null, minCreatedAt: null);
      yield FacebookLiveLoaded(facebookLiveVideo: facebookLiveVideo, facebookLiveFeeds: feeds, hasReachMax: feeds.isEmpty);
      print('addd');

      this.add(SocketPostSubscribe(facebookLiveVideo: facebookLiveVideo));
      this.add(SocketFacebookVideoSubscribe());
    } catch (e) {
      print('E001');
      print(e.toString());
    }

  }

  Stream<FacebookLiveState> _mapFetchFeedsMore(FetchFeedsMore event) async* {
    yield FacebookLiveProcessing();

    try {
      if (authLayoutBloc.shop == null) {
        return;
      }

      List<FacebookLiveFeed> feeds = await facebookLiveFeedRepository.fetchFeed(shopId: authLayoutBloc.shop!.id!, postId: event.facebookLiveVideo.id!, minId: event.facebookLiveFeeds.last.id, minCreatedAt: event.facebookLiveFeeds.last.createdAt);
      yield FacebookLiveLoaded(facebookLiveVideo: event.facebookLiveVideo, facebookLiveFeeds: event.facebookLiveFeeds + feeds, hasReachMax: feeds.isEmpty);
    } catch (e) {
      print('E002');
      print(e.toString());
    }
  }

  Stream<FacebookLiveState> _mapSearchProduct(SearchProduct event) async* {
    yield FacebookLiveProcessing();
    try {
      if (selectProduct != null) {
        socket.off('item-${selectProduct!.id}');
      }

      List<Product> products = await productRepository.fetchProducts(shopId: authLayoutBloc.shop!.id!, refNo: event.refno);
      Product? product = products.isNotEmpty ? products.first : null;

      selectProduct = product;

      if (product != null && product.items != null) {
        socket.on('item-${product.id}', (data) {
          // print(data);
          add(SyncItem(data: data['item']));
        });
      }
      yield ProductLoaded(product: product);

      // Search for already in live video product;
      if (await facebookLiveVideoProductRepository.isExists(shopId: authLayoutBloc.shop!.id!, facebookVideoId: event.facebookVideoId, productId: product!.id!)) {
        yield SellProductLoaded(product: product);
      }

    } catch (e) {
      print('E003');
      print(e.toString());
    }
  }
}