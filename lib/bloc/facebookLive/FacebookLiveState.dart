import 'package:equatable/equatable.dart';

import '../../models/FacebookLiveVideo.dart';
import '../../models/FacebookLiveFeed.dart';
import '../../models/Product.dart';
import '../../models/Item.dart';

abstract class FacebookLiveState extends Equatable {
  const FacebookLiveState();

  @override
  List<Object> get props => [];
}

class FacebookLiveUninitialized extends FacebookLiveState {}
class FacebookLiveProcessing extends FacebookLiveState {}
class FacebookLiveProcessingBackGround extends FacebookLiveState {}
class FacebookLiveFeedSync extends FacebookLiveState {
  final FacebookLiveFeed facebookLiveFeed;

  FacebookLiveFeedSync({required this.facebookLiveFeed});
}

class ItemSync extends FacebookLiveState {
  final Item item;

  ItemSync({required this.item});
}

class FacebookLiveLoaded extends FacebookLiveState {
  final FacebookLiveVideo facebookLiveVideo;
  final List<FacebookLiveFeed> facebookLiveFeeds;
  final bool hasReachMax;

  FacebookLiveLoaded({required this.facebookLiveVideo, required this.facebookLiveFeeds, required this.hasReachMax});
}

class ProductLoaded extends FacebookLiveState {
  final Product? product;

  ProductLoaded({required this.product});
}

class HideShowUpdated extends FacebookLiveState {
  final hideOverlay;

  HideShowUpdated({required this.hideOverlay});
}

class SellProductLoaded extends FacebookLiveState {
  final Product product;
  SellProductLoaded({required this.product});
}

class FailProcess extends FacebookLiveState {
  final String error;

  FailProcess({required this.error});
}
