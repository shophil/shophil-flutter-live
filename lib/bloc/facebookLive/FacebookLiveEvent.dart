import 'package:equatable/equatable.dart';

import '../../models/FacebookLiveFeed.dart';
import '../../models/FacebookLiveVideo.dart';
import '../../models/Product.dart';
import '../../models/ProductImage.dart';

abstract class FacebookLiveEvent extends Equatable {
  const FacebookLiveEvent();

  @override
  List<Object> get props => [];
}

class SocketInit extends FacebookLiveEvent {
  SocketInit();
}

class SocketDisconnect extends FacebookLiveEvent {
  SocketDisconnect();
}

class SocketPostSubscribe extends FacebookLiveEvent {
  final FacebookLiveVideo facebookLiveVideo;

  SocketPostSubscribe({required this.facebookLiveVideo});
}

class SocketFacebookVideoSubscribe extends FacebookLiveEvent {
  SocketFacebookVideoSubscribe();
}

class SyncMessage extends FacebookLiveEvent {
  final dynamic data;

  SyncMessage({required this.data});
}

class SyncItem extends FacebookLiveEvent {
  final dynamic data;

  SyncItem({required this.data});
}


class LiveInit extends FacebookLiveEvent {
  @override
  String toString() => 'LiveInit';
}

class FetchFeedsMore extends FacebookLiveEvent {
  final FacebookLiveVideo facebookLiveVideo;
  final List<FacebookLiveFeed> facebookLiveFeeds;

  FetchFeedsMore({required this.facebookLiveVideo, required this.facebookLiveFeeds});

  @override
  String toString() => 'FetchFeedsMore';
}

class SearchProduct extends FacebookLiveEvent {
  final String refno;
  final String facebookVideoId;

  SearchProduct({required this.refno, required this.facebookVideoId});
}

class DisplayLiveObs extends FacebookLiveEvent {
  final String productId;
  final String facebookVideoId;

  DisplayLiveObs({required this.productId, required this.facebookVideoId});
}

class HideLiveObs extends FacebookLiveEvent {}

class SellThisProduct extends FacebookLiveEvent {
  final FacebookLiveVideo facebookLiveVideo;
  final Product product;

  SellThisProduct({required this.facebookLiveVideo, required this.product});
}

class ShowImageObs extends FacebookLiveEvent {
  final FacebookLiveVideo facebookLiveVideo;
  final Product product;
  final ProductImage productImage;

  ShowImageObs({required this.facebookLiveVideo, required this.product, required this.productImage});
}

class HideImageObs extends FacebookLiveEvent {
  final FacebookLiveVideo facebookLiveVideo;
  final Product product;
  HideImageObs({required this.facebookLiveVideo, required this.product});
}