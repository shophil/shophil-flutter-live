import 'package:equatable/equatable.dart';

abstract class LoginMainEvent extends Equatable {
  LoginMainEvent();

  @override
  List<Object> get props => [];
}

class LoginButtonPressed extends LoginMainEvent {
  final String username;
  final String password;

  LoginButtonPressed({required this.username, required this.password});
}

class FacebookButtonPressed extends LoginMainEvent {
  FacebookButtonPressed();
}