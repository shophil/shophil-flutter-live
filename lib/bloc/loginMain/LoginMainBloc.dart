import 'dart:async';
import 'package:bloc/bloc.dart';

import '../../managers/AuthenticationManager.dart';
import '../authenticate/AuthenticationBloc.dart';
import '../authenticate/AuthenticationEvent.dart';
import '../../repositories/UserRepository.dart';
import './LoginMain.dart';

class LoginMainBloc extends Bloc<LoginMainEvent, LoginMainState> {
  final AuthenticationManager authenticationManager;
  final AuthenticationBloc authenticationBloc;
  final UserRepository userRepository;

  LoginMainBloc({required this.authenticationManager, required this.authenticationBloc, required this.userRepository}) : super(LoginMainInitial());

  @override
  Stream<LoginMainState> mapEventToState(LoginMainEvent event) async* {
    if (event is LoginButtonPressed) {
      yield* _mapLoginButtonPressed(event);
    } else if (event is FacebookButtonPressed) {
      yield* _mapFacebookButtonPressed(event);
    }
  }

  Stream<LoginMainState> _mapLoginButtonPressed(LoginButtonPressed event) async* {
    yield LoginMainLoading();
    try {
      String token = await authenticationManager.authenticate(username: event.username, password: event.password);
      authenticationBloc.add(LoggedIn(token: token));
      yield LoginMainSuccess();
    } catch (e) {
      yield LoginMainFail();
    }
  }

  Stream<LoginMainState> _mapFacebookButtonPressed(FacebookButtonPressed event) async* {
    yield LoginMainLoading();

    try {
      String token = await authenticationManager.facebookAuthenticate();
      authenticationBloc.add(LoggedIn(token: token));
       yield LoginMainSuccess();
    } catch (e) {
      print(e.toString());
      yield LoginMainFail();
    }
  }
}