import 'dart:async';
import 'package:bloc/bloc.dart';

import './AuthLayout.dart';
import '../../repositories/ShopRepository.dart';
import '../../models/Shop.dart';

class AuthLayoutBloc extends Bloc<AuthLayoutEvent, AuthLayoutState> {
  final ShopRepository shopRepository;
  Shop? shop;

  AuthLayoutBloc({required this.shopRepository}): super(SelectShop());


  @override
  Stream<AuthLayoutState> mapEventToState(
    AuthLayoutEvent event,
  ) async* {
    if (event is ChangeToDashboard) {
      shop = event.shop;
      yield Dashboard();
    }else if(event is ChangeToSelectShop){
      await shopRepository.removeSelectShop();
      yield SelectShop();
    }else{
      yield SelectShop();
    }
  }
}