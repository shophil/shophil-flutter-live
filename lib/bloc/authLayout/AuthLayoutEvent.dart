import 'package:equatable/equatable.dart';

import '../../models/Shop.dart';

abstract class AuthLayoutEvent extends Equatable {
  const AuthLayoutEvent();

  @override
  List<Object> get props => [];
}

class HomeStart extends AuthLayoutEvent{
  @override
  String toString() => 'HomeStart';
}

class ChangeToSelectShop extends AuthLayoutEvent {
  @override
  String toString() => 'Select shop page';
}

class ChangeToDashboard extends AuthLayoutEvent {
  final Shop shop;

  ChangeToDashboard({
    required this.shop,
  });

  @override
  String toString() => 'Dashboard page';
}

