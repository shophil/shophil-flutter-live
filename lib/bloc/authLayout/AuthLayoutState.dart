import 'package:equatable/equatable.dart';

abstract class AuthLayoutState extends Equatable {
  @override
  List<Object> get props => [];
}

class SelectShop extends AuthLayoutState {
  @override
  String toString() => 'Home select shop';
}

class Dashboard extends AuthLayoutState {
  @override
  String toString() => 'Home dashboard';
}
