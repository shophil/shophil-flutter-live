import 'package:equatable/equatable.dart';

abstract class MainState extends Equatable {
  const MainState();

  @override
  List<Object> get props => [];
}

class TabChanged extends MainState {
  final int index;

  TabChanged({required this.index});

  @override
  String toString() => 'MainDashboard state';

  @override
  List<Object> get props => [index];
}
