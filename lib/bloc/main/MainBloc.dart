import 'dart:async';
import 'package:bloc/bloc.dart';

import './Main.dart';

class MainBloc extends Bloc<MainEvent, MainState> {
  MainBloc(): super(TabChanged(index: 0));

  @override
  Stream<MainState> mapEventToState(MainEvent event) async* {
    if (event is ChangeToProfile) {
      yield TabChanged(index: 1);
    }
    if (event is ChangeToLive) {
      yield TabChanged(index: 0);
    }
  }
}