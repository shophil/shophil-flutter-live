import 'package:equatable/equatable.dart';

abstract class MainEvent extends Equatable {
  @override
  List<MainEvent> get props => [];
}

class ChangeToLive extends MainEvent {
  @override
  String toString() => 'Select live page';
}

class ChangeToProfile extends MainEvent {
  @override
  String toString() => 'Select profile page';
}



